package org.gopas.api;

public class PersonsInCitiesDto {

    private String city;
    private long number;

    public PersonsInCitiesDto() {
        //
    }

    public PersonsInCitiesDto(String city, long number) {
        this.city = city;
        this.number = number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "PersonsInCitiesDto{" +
                "city='" + city + '\'' +
                ", number=" + number +
                '}';
    }
}
