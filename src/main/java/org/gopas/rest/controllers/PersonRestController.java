package org.gopas.rest.controllers;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.gopas.api.PersonCreateDto;
import org.gopas.api.PersonFullViewDto;
import org.gopas.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin("*")
@Api(value = "/persons", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping(path = "/persons")
public class PersonRestController {

    private PersonService personService;

    @Autowired
    public PersonRestController(PersonService personService) {
        this.personService = personService;
    }

    //@RequestParam anotace pro predavani parametru
    // sofistikovaneji QueryDsl

    @ApiOperation(
            httpMethod = "GET",
            value = "Get Person by Id.",
            response = PersonFullViewDto.class,
            nickname = "findPersonById",
            produces = "application/json"
    )
    // HTTP GET ~/persons/{id}
    @GetMapping(path = "/{id}")
    public PersonFullViewDto findById(
            @ApiParam(name = "id")
            @PathVariable("id") Long id) {
        return personService.findById(id);
    }

    @PostMapping
    public void createPerson(@Valid @RequestBody PersonCreateDto personCreateDto) {
        personService.createPerson(personCreateDto);
    }

    // vytvorene repo projektu: https://gitlab.com/SedaQ/spring-demo
    // zde to budu updatovat, at si to nemusime posilat sem a tam (da se i online prohlizet..)

    // POST
    // samostatne UPDATE
    // samostatne findAll HTTP Resource
}
