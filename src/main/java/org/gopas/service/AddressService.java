package org.gopas.service;

import org.gopas.data.entities.Address;
import org.gopas.data.entities.Person;
import org.gopas.data.repositories.AddressRepository;
import org.gopas.service.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AddressService {

    private AddressRepository addressRepository;

    @Autowired
    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public Address findById(Long id) {
        Address address = addressRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("The address with id " + id + " was not found."));
        return address;
    }
}
