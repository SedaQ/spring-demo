package org.gopas.service;

import org.gopas.api.PersonDto;
import org.gopas.api.PersonFullViewDto;
import org.gopas.data.entities.Person;
import org.gopas.data.repositories.PersonRepository;
import org.gopas.service.mapstruct.mappers.PersonMapper;

public class PersonServiceProxy extends PersonService {

    public PersonServiceProxy(PersonRepository personRepository, PersonMapper personMapper) {
        super(personRepository, personMapper);
    }

    public Person difficult(Long id) {
        // begin.transaction ()
        return super.difficult(id);
        //
    }

    public PersonFullViewDto findById(Long id) {
        // neco ..
        return super.findById(id);
        //
    }

    // findAll
}
