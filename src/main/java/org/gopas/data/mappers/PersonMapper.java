package org.gopas.data.mappers;

import org.gopas.api.PersonDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonMapper implements RowMapper<PersonDto> {

    @Override
    public PersonDto mapRow(ResultSet resultSet, int i) throws SQLException {
        PersonDto person = new PersonDto();
        person.setId(resultSet.getLong("id_person"));
        person.setEmail(resultSet.getString("email"));
        person.setNickname(resultSet.getString("nickname"));
        return person;
    }
}
