package org.gopas.api;

import org.gopas.utils.annotations.PasswordValidator;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class PersonCreateDto {

    @NotNull(message = "Field surname is not provided.")
    @NotEmpty(message = "Field surname is not provided.")
    private String surname;
    @PasswordValidator
    private String pwd;
    @NotNull(message = "Field email is not provided.")
    @NotEmpty(message = "Field email is not provided.")
    @Email(message = "Field email is not valid.")
    private String email;
    private String nickname;
    private String firstName;
    private LocalDate birthday;

    public PersonCreateDto() {
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "PersonCreateDto{" +
                "surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", nickname='" + nickname + '\'' +
                ", firstName='" + firstName + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
