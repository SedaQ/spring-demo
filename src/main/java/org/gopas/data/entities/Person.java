package org.gopas.data.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@NamedQueries(
        @NamedQuery(
                name = "Person.findAllNamedQuery",
                query = "SELECT p FROM Person p"
        )
)
@Entity
@Table(name = "person")
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_person")
    private Long id;
    @Column(name = "surname")
    private String surname;
    @Column(name = "pwd")
    private String pwd;
    @Column(name = "email", nullable = false, unique = true)
    private String email;
    @Column(name = "nickname")
    private String nickname;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "birthday")
    private LocalDate birthday;
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "id_address")
    private Address address;
    @ManyToMany(mappedBy = "persons", fetch = FetchType.LAZY)
    private Set<Meeting> meetings = new HashSet<>();
    @OneToMany(mappedBy = "person", fetch = FetchType.LAZY)
    private Set<PersonHasRole> personHasRoles = new HashSet<>();

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<PersonHasRole> getPersonHasRoles() {
        return Collections.unmodifiableSet(personHasRoles);
    }

    public void setPersonHasRoles(Set<PersonHasRole> personHasRoles) {
        this.personHasRoles = personHasRoles;
    }

    public Set<Meeting> getMeetings() {
        return Collections.unmodifiableSet(meetings);
    }

    public void setMeetings(Set<Meeting> meetings) {
        this.meetings = meetings;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(getEmail(), person.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getEmail());
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", nickname='" + nickname + '\'' +
                ", firstName='" + firstName + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
