package org.gopas.data.repositories;

import org.gopas.api.PersonDto;
import org.gopas.api.PersonsInCitiesDto;
import org.gopas.data.entities.Person;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

// SELECT * FROM person p JOIN address a ON p.id_address = a.id_address WHERE p.id_person = ?;
// SELECT p FROM Person p JOIN p.address a JOIN a.something s WHERE p.id_person = ?"

@Repository
public interface PersonRepository extends JpaRepository<Person, Long>, PersonRepositoryCustom {

    // TASK spocitat pocet zamestnancu v danych mestech
    // vysledek je seznam mest a pocet osob v danem meste
    // JPQL -- GROUP BY.., vrat pouze agregaci dat
//    SELECT a.city, COUNT(p.id_person) FROM person p JOIN address a ON p.id_address = a.id_address GROUP BY a.city;
    @Query("SELECT new org.gopas.api.PersonsInCitiesDto(a.city, COUNT(p.id))" +
            " FROM Person p" +
            " LEFT JOIN p.address a" +
            " GROUP BY a.city")
    List<PersonsInCitiesDto> countPersonInCities();


    @Query("SELECT p FROM Person p WHERE p.email = :email")
    Person findByEmail(@Param("email") String email); // dokaze vygenerovat spravnou query...

    //    @EntityGraph(attributePaths = {"address"})
//    @Query("SELECT p FROM Person p JOIN FETCH p.address a JOIN FETCH p.personHasRoles phr")
    @Query("SELECT p FROM Person p JOIN FETCH p.address a")
    List<Person> findAllWithAddress();

    List<Person> findAllNamedQuery();

    // findAll
    @Query("SELECT new org.gopas.api.PersonDto(p.email,p.nickname) FROM Person p WHERE p.id = :id")
    PersonDto findByOwnConstructor(@Param("id") Long id);

    @Query("SELECT p FROM Person p JOIN FETCH p.address a WHERE p.id = :id")
    Optional<Person> findByIdWithAddress(@Param("id") Long id);

//    @PersistenceContext // why not Autowired?
//    private EntityManager entityManager;
//
//    public Person findById(Long id) {
//        return entityManager.find(Person.class, id);
//    }
//
//    public Person findByEmail(String email) {
//        return entityManager.createQuery("SELECT p FROM Person p WHERE p.email = :email", Person.class)
//                .setParameter("email", email)
//                .getSingleResult();
//    }
//
//    public List<Person> findAll() {
//        return entityManager.createQuery("SELECT p FROM Person p", Person.class)
//                .getResultList();
//    }
//
//    public void createPerson(Person person) {
//        // another methods...
//        entityManager.persist(person);
//    }
}
