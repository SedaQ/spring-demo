package org.gopas.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDate;

@ApiModel(value = "PersonFullViewDto", description = "Full view of particular person.")
public class PersonFullViewDto {

    @ApiModelProperty(name = "id", position = 1, example = "1")
    private Long id;
    @ApiModelProperty(name = "email", example = "johndoe@email.cz")
    private String email;
    @ApiModelProperty(name = "nickname", example = "SedaQ")
    private String nickname;
    private String firstName;
    private LocalDate birthday;
    private PersonFullViewAddressDto address;

    public PersonFullViewDto() {

    }

    public PersonFullViewAddressDto getAddress() {
        return address;
    }

    public void setAddress(PersonFullViewAddressDto address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "PersonFullViewDto{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", nickname='" + nickname + '\'' +
                ", firstName='" + firstName + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
