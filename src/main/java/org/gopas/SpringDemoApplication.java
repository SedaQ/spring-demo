package org.gopas;

import org.gopas.api.PersonDto;
import org.gopas.api.PersonFullViewDto;
import org.gopas.api.PersonsInCitiesDto;
import org.gopas.data.entities.Address;
import org.gopas.data.entities.Person;
import org.gopas.data.repositories.PersonRepository;
import org.gopas.rest.config.SwaggerConfig;
import org.gopas.service.PersonService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;

import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class SpringDemoApplication {

    public static void main(String[] args) {
        // vytvoreni aplikacniho kontextu
        ApplicationContext applicationContext =
                SpringApplication.run(SpringDemoApplication.class, args);

        PersonService personService = applicationContext.getBean(PersonService.class);
        // t1
        PersonFullViewDto personDto = personService.findById(5L);
        System.out.println(personDto);

//
//        List<PersonsInCitiesDto> personsInCitiesDto = personService.personsInCitiesDto();
//        personsInCitiesDto.forEach(person -> {
//            System.out.println(person);
//        });

    }

}
