package org.gopas.service;

import org.gopas.api.PersonCreateDto;
import org.gopas.api.PersonDto;
import org.gopas.api.PersonFullViewDto;
import org.gopas.api.PersonsInCitiesDto;
import org.gopas.data.entities.Person;
import org.gopas.data.repositories.PersonRepository;
import org.gopas.service.exceptions.ResourceNotFoundException;
import org.gopas.service.mapstruct.mappers.PersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

// interface PersonService
// class PersonServiceImpl

// ... @Autowired private PersonService personService;

@Service
@Transactional
public class PersonService {

    @Autowired
    private PersonService self;

    private final PersonRepository personRepository;
    private PersonMapper personMapper;

    @Autowired
    public PersonService(PersonRepository personRepository, PersonMapper personMapper) {
        this.personRepository = personRepository;
        this.personMapper = personMapper;
    }

    public List<PersonsInCitiesDto> personsInCitiesDto() {
        return personRepository.countPersonInCities();
    }

    // 1 ?
    // 2 ?
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public Person difficult(Long id) {
        this.findAll();
//        return self.findById(id); // to call inner method using proxy object
        return personRepository.findById(id).
                orElseThrow(() -> new ResourceNotFoundException("The person with id " + id + " was not found."));
    }

    public void createPerson(PersonCreateDto personCreateDto) {
        Person person = personMapper.mapCreateDtoToEntity(personCreateDto);
        personRepository.save(person);
    }

    @Transactional(readOnly = true)
    public PersonDto findByIdComplex(Long id) {
        return personRepository.findByIdComplex(id);
    }

    @Transactional(readOnly = true)
    public PersonFullViewDto findById(Long id) {
        Person person = personRepository.findByIdWithAddress(id).
                orElseThrow(() -> new ResourceNotFoundException("The person with id " + id + " was not found."));

        return personMapper.mapToFullViewDto(person);
//        return mapToPersonDto(person); // rucni mapovani... pracne ...
    }

    private PersonDto mapToPersonDto(Person person) {
        PersonDto personDto = new PersonDto();
        personDto.setEmail(person.getEmail());
        personDto.setId(person.getId());
        personDto.setNickname(person.getNickname());
        personDto.setBirthday(person.getBirthday());
        personDto.setFirstName(person.getFirstName());
        return personDto;
    }

    // N+1
    @Transactional
    public List<Person> findAll() {
        List<Person> persons = personRepository.findAllWithAddress();
        persons.forEach(person -> {
            System.out.println(person.getAddress());
        });
        return persons;
    }
}
