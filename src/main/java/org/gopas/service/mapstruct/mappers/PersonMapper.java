package org.gopas.service.mapstruct.mappers;

import org.gopas.api.PersonCreateDto;
import org.gopas.api.PersonDto;
import org.gopas.api.PersonFullViewDto;
import org.gopas.data.entities.Person;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PersonMapper {

    PersonDto mapToDto(Person person);

    PersonFullViewDto mapToFullViewDto(Person person);

    Person mapCreateDtoToEntity(PersonCreateDto personCreateDto);

    default Person mapToEntity(PersonDto personDto) {
        Person person = new Person();
        person.setEmail(personDto.getEmail());
        // other mappings
        return person;
    }
}
