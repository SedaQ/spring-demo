package org.gopas.data.repositories;

import org.gopas.api.PersonDto;
import org.gopas.data.mappers.PersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class PersonRepositoryImpl implements PersonRepositoryCustom {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public PersonRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public PersonDto findByIdComplex(Long id) {
        return jdbcTemplate.queryForObject(
                "SELECT id_person, email, nickname FROM person p WHERE p.id_person = ?",
                new Object[]{id},
                new PersonMapper());
    }
}
