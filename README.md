# Spring demo

# Build and Run (Using Docker)
Build the project:
```
docker build -t sedaq/spring-demo .
```
Run the project:
```
docker run --name spring-demo -it -p 8081:8081 sedaq/spring-demo
```