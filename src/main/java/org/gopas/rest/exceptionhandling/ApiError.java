package org.gopas.rest.exceptionhandling;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ApiError {

    private String message;
    private long timestamp;
    private String path;
    private HttpStatus status;
    private List<String> errors;

    public ApiError() {
        message = "";
    }

    public ApiError(String message, long timestamp, String path, HttpStatus status, List<String> errors) {
        this.message = message;
        this.timestamp = timestamp;
        this.path = path;
        this.status = status;
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
