package org.gopas.data.repositories;

import org.gopas.api.PersonDto;

public interface PersonRepositoryCustom {

    PersonDto findByIdComplex(Long id);
}
