package org.gopas.rest.exceptionhandling;

import org.gopas.service.exceptions.ResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.List;

@RestControllerAdvice
public class CustomRestExceptionHandler {

    public static final UrlPathHelper URL_PATH_HELPER = new UrlPathHelper();

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Object> handleResourceNotFoundException(
            ResourceNotFoundException ex,
            WebRequest webRequest,
            HttpServletRequest req) {
        ApiError apiError = fillUpApiError(ex, webRequest, req);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolationException(
            ConstraintViolationException ex,
            WebRequest webRequest,
            HttpServletRequest req) {
        ApiError apiError = fillUpApiError(ex, webRequest, req);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex, WebRequest webRequest, HttpServletRequest req) {
        List<ObjectError> objectErrorList = ex.getBindingResult().getAllErrors();
        ApiError apiError = new ApiError();
        objectErrorList.forEach(error -> {
            apiError.setMessage(apiError.getMessage() + "[" + ((FieldError) error).getField() + "]" + " " + error.getDefaultMessage());
        });
        apiError.setStatus(HttpStatus.NOT_FOUND);
        apiError.setTimestamp(System.currentTimeMillis());
        apiError.setPath(URL_PATH_HELPER.getRequestUri(req));
        apiError.setErrors(null);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex, WebRequest webRequest, HttpServletRequest req) {
        ApiError apiError = fillUpApiError(ex, webRequest, req);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleInternalServerError(Exception ex,
                                                            WebRequest webRequest,
                                                            HttpServletRequest req) {
        ApiError apiError = fillUpApiError(ex, webRequest, req);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    private ApiError fillUpApiError(Exception ex, WebRequest webRequest, HttpServletRequest req) {
        ApiError apiError = new ApiError();
        apiError.setMessage(ex.getMessage());
        apiError.setStatus(HttpStatus.NOT_FOUND);
        apiError.setTimestamp(System.currentTimeMillis());
        apiError.setPath(URL_PATH_HELPER.getRequestUri(req));
        apiError.setErrors(null);
        return apiError;
    }
}
