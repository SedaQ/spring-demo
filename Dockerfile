# buildInternalMessage stage
FROM maven:3.6.3-openjdk-11-slim AS build

LABEL maintainer="pavelseda@email.cz"
ENV SERVICE_PORT 8081

COPY /etc/spring-demo.properties /app/etc/spring-demo.properties
COPY src/ /app/src/
COPY pom.xml /app/pom.xml

WORKDIR /app

RUN mvn clean package -DskipTests
RUN cp /app/target/spring-demo-*.jar /app/spring-demo.jar

# java stage
FROM openjdk:11.0.9.1-jdk-slim AS openjdk

COPY --from=build /app/etc/spring-demo.properties /app/etc/spring-demo.properties
COPY --from=build /app/spring-demo*.jar /app/spring-demo.jar

EXPOSE $SERVICE_PORT
WORKDIR /app
ENTRYPOINT ["java", "-Dspring.config.location=/app/etc/spring-demo.properties","-jar","/app/spring-demo.jar"]
